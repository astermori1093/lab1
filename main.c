#include <pic14/pic12f675.h>

//Configuramos el registro CONFIG
typedef unsigned int word;
word __at 0x2007 __CONFIG =(_WDT_OFF & _MCLRE_OFF);

// definimos las funciones que implementamos
void delay (unsigned inttiempo);

void LEDS_OFF(void);

void main(void){
//Ponemos todos los pines como salida excepto el pin que lee el estado 
//del botón
    TRISIO=0x08;
//Deshabilitamos todas las salidas 
    GPIO=0x00;
//Convertines pines de analógico a digital
    ANSEL=0x00;
//Establecemos dos contadores
//time para el tiempo de delay
    unsigned int time=200;
//cara_D determinara cual estado tendrá nuestra maquina de estado
    unsigned int cara_D=0;
    unsigned int tiempo=0;
//Creamos un bucle infinito
    while(1){  
        //Creamos un bucle for que acabara cuando presionen el botón generando asi un numero pseudo aleatorio
        for(unsigned int j=0;j<252;j++){
            //Aumentamos el contador
            tiempo++;
            //Si el botón se pulsa se accederá a esta sección del código
            if(GPIO3==1){
                //Determinamos cual de los 6 estados se dará, calculando el modulo entre la variable tiempo y el numero seis
                cara_D=tiempo%6;
                //Se determinara a que caso se accederá
                switch (cara_D)
                {
                //caso 0: se encenderán  3 diodos LEDS
                case 0:
                    GPIO|=0b00000011; 
                     
                    delay(time);
                    LEDS_OFF();
                    break;
                //caso 1: se encenderán  1 diodo LED
                case 1:
                    GPIO|=0b00000001;
                    delay(time);
                    LEDS_OFF();
                    break;
                //caso 2: se encenderán  2 diodos LEDS
                case 2:
                    GPIO|=0b00000010;
                    delay(time);
                    LEDS_OFF();
                    break;
                //caso 3: se encenderán  4 diodos LEDS
                case 3:
                    GPIO|=0b00000111; 
                    delay(time);
                    LEDS_OFF();
                    break;
                //caso 4: se encenderán  5 diodos LEDS
                case 4:
                    GPIO|=0b00010111; 
                    delay(time);
                    LEDS_OFF();
                    break;
                //caso default: se encenderán  6 diodos LEDS
                default:
                    GPIO|=0b00110111; 
                    delay(time);
                    LEDS_OFF();
                    break;
                }
            }
        }
    //Se restablece el tiempo  a cero
    tiempo=0;
    }
}
//Esta función pone en bajo todos los pines GPIO de salida
void LEDS_OFF(void){
    GPIO=0x00;

}
//Esta función genera un pequeño retraso en la ejecución de este programa
void delay(unsigned int inttiempo)
{
	for(unsigned int i=0;i<inttiempo;i++){
	  for(unsigned int j=0;j<1275;j++){

      }
    }
}



